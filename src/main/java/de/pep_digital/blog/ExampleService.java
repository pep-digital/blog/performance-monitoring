package de.pep_digital.blog;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * Both methods are restarted after 5 seconds
 */
@Service
public class ExampleService {
    private final Random random = new Random();

    @TimedMethod
    @Scheduled(fixedDelay = 5000)
    public void foo() throws InterruptedException {
        // Sleep between 1 and 5 seconds
        final int s = random.nextInt(5) + 1;
        Thread.sleep(s * 1000);
        System.out.println("Method foo: " + s + "s");
    }

    @TimedMethod
    @Scheduled(fixedDelay = 5000)
    public void bar() throws InterruptedException {
        // Sleep between 5 and 10 seconds
        final int s = random.nextInt(6) + 5;
        Thread.sleep(s * 1000);
        System.out.println("Method bar: " + s + "s");
    }
}
