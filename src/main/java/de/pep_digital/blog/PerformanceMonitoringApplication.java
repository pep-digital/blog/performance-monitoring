package de.pep_digital.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PerformanceMonitoringApplication {

    public static void main(String[] args) {
        SpringApplication.run(PerformanceMonitoringApplication.class, args);
    }

}
