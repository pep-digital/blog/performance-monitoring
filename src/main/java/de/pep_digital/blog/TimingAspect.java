package de.pep_digital.blog;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TimingAspect {
    private final MeterRegistry meterRegistry;

    public TimingAspect(MeterRegistry meterRegistry) {this.meterRegistry = meterRegistry;}

    @Around("@annotation(TimedMethod)")
    public Object timeMethod(ProceedingJoinPoint pjp) {
        final var methodName = "performance-monitoring_method_" + pjp.getSignature().getName();
        final var timer = Timer.builder(methodName).description("Total time of " + pjp.getSignature().getName() + " method").register(meterRegistry);

        try {
            return timer.recordCallable(() -> {
                try {
                    return pjp.proceed();
                } catch (Throwable t) {
                    // Handle or log the exception as needed
                    throw new RuntimeException(t);
                }
            });
        } catch (Exception e) {
            // Handle or log the exception as needed
            throw new RuntimeException(e);
        }
    }
}