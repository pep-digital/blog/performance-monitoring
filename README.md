# Performance monitoring with annotations and Prometheus

This is an example project for monitoring performance in Spring Boot.

See https://pep-digital.de/blog/performance-monitoring for the related bog post.